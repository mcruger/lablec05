package edu.uchicago.gerber.prolec05.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link edu.uchicago.gerber.prolec05.app.UpperFragment.OnUpperFragListener} interface
 * to handle interaction events.
 *
 */
public class UpperFragment extends Fragment {

    private OnUpperFragListener mListener;
    private Button mButton;

    public UpperFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upper, container, false);
        mButton = (Button) view.findViewById(R.id.button_set);
        final EditText editText = (EditText) view.findViewById(R.id.edit_color);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onUpperFragmentInteraction(editText.getText().toString());
                mButton.setEnabled(false);
            }
        });


        return view;
    }

    public void enableButton(){
        mButton.setEnabled(true);

    }





    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnUpperFragListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnUpperFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnUpperFragListener {
        // TODO: Update argument type and name
        public void onUpperFragmentInteraction(String color);
    }

}
