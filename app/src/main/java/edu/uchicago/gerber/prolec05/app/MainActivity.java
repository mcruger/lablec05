package edu.uchicago.gerber.prolec05.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity implements UpperFragment.OnUpperFragListener, LowerFragment.OnLowerFragListener {

    private LowerFragment mLowerFragment;
    private UpperFragment mUpperFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (savedInstanceState == null) {
            mLowerFragment = new LowerFragment();
            mUpperFragment = new UpperFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_upper, mUpperFragment)
                    .add(R.id.container_lower, mLowerFragment)
                    .commit();
        } else {
            mLowerFragment = (LowerFragment) getSupportFragmentManager().findFragmentById(R.id.container_upper);
            mUpperFragment = (UpperFragment) getSupportFragmentManager().findFragmentById(R.id.container_lower);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //upper fragment
    @Override
    public void onUpperFragmentInteraction(String color) {

        if (mLowerFragment != null)
             mLowerFragment.setMe(color);

    }

    //lower fragment
    @Override
    public void onLowerFragmentInteraction() {

        if (mUpperFragment != null)
            mUpperFragment.enableButton();
    }
}
