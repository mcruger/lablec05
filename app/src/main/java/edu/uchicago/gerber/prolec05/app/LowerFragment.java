package edu.uchicago.gerber.prolec05.app;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

//create two way commuication so that when you click the lowerFragment layout, it set's the button text to soemthing interesting.
//custom dialogs http://stackoverflow.com/questions/13341560/how-to-create-a-custom-dialog-box-in-android

public class LowerFragment extends Fragment {

    private OnLowerFragListener mLowerFragListener;
   // private OnUpperFragListener mUpperFragListener;

    private FrameLayout mColorFrame;

    public LowerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mLowerFragListener = null;
       // mUpperFragListener = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mLowerFragListener = (OnLowerFragListener) activity;
          //  mUpperFragListener = (OnUpperFragListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnUpperFragListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lower, container, false);
        mColorFrame = (FrameLayout) view.findViewById(R.id.color_frame);
        mColorFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mColorFrame.setBackgroundColor(Color.GRAY);
                mLowerFragListener.onLowerFragmentInteraction();
            }
        });


        return view;

    }

    public void setMe(String color){
          mColorFrame.setBackgroundColor(Color.parseColor(color));

    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLowerFragListener {
        public void onLowerFragmentInteraction();
    }

}
